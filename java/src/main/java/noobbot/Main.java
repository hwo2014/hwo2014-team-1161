package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

// VTR - done configurating bitbucket :D

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper2 msgFromServer = gson.fromJson(line, MsgWrapper2.class);
            if (msgFromServer.msgType.equals("carPositions") || msgFromServer.msgType.equals("gameStart")) {
                send(new Throttle(0.62, msgFromServer.tick));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg2 msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

abstract class SendMsg2 {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper2(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    protected abstract int msgTick();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class MsgWrapper2 {
    public final String msgType;
    public final Object data;
    public final int tick;

    MsgWrapper2(final String msgType, final Object data, final int tick) {
        this.msgType = msgType;
        this.data = data;
        this.tick = tick;
    }

    public MsgWrapper2(final SendMsg2 sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.msgTick());
    }
}

class Join extends SendMsg2 {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

    @Override
    protected int msgTick() {
        return 0;
    }
}

class Ping extends SendMsg2 {
    @Override
    protected String msgType() {
        return "ping";
    }

    @Override
    protected int msgTick() {
        return 0;
    }
}

class Throttle extends SendMsg2 {
    private double value;
    private int tick;

    public Throttle(double value, int tick) {
        this.value = value;
        this.tick = tick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

    @Override
    protected int msgTick() {
        return tick;
    }
}